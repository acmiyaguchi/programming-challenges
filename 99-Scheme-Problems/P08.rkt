#lang racket/base
;; P08 - Eliminate consecutive duplicates of list elements.
(define (compress ls)
  (cond ((null? (cdr ls)) 
         (cons (car ls) '()))
        ((equal? (car ls) (car (cdr ls)))
         (compress (cdr ls)))
        (else
         (cons (car ls) (compress (cdr ls))))
        )
  )

;;Testing
(equal? (compress '(a a a a b c c a a d e e e))
        '(a b c a d e))
(equal? (compress '(a a a a)) '(a))
(equal? (compress '((a b) (a b) b b)) '((a b) b))
