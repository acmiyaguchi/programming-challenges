#lang racket/base
;; P16 - Drop every N'th element from a list

;;helper function
(define (drophelper ls n1 n2)
  (cond ((null? ls)
         '())
        ((zero? (- n1 n2))
         (drophelper (cdr ls) n1 1))
        (else
         (cons (car ls) (drophelper (cdr ls) n1 (+ n2 1))))
        )
  )

(define (drop ls n)
  (drophelper ls n 1)
  )

;;Testing
(equal? (drop '(a b c d) 2) '(a c))
(equal? (drop '(a b c d e f g h) 3) '(a b d e g h))