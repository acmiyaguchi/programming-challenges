#lang racket/base
;; P04 - Find the number of elements of a list

(define (list-len ls)
  (if (null? ls)
      0
      (+ 1 (list-len (cdr ls)))
      )
  )

;;Testing
(equal? (list-len '(a b c d)) 4)
(equal? (list-len '(a (b c) d)) 3)