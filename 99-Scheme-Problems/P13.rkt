#lang racket/base
;; P13 - Run-length encoding of a list (direct solution)
(define (encode-direct ls)
  (cond ((null? ls)
         '())
        ((equal? (car ls) (cadr ls)) (+ 1 )
        (else
         (cons (
      

;;Testing
(encode-direct '(a a a b b c))
(equal? (encode-direct '(a a a b b c)) ((3 a) (2 b) c))
