#lang racket/base
;; Rotate a list N places to the left

;;Load split for dividing a list
(load "P17.scm")

(define (rotate ls n)
  (cond ((zero? n) 
         ls)
        ((> n 0)
         ;;Rotate the list left
         (let ((lst (split ls n)))
           (append (cadr lst) (car lst))))
        ((< n 0)
         ;;Rotate the list right
         (let ((lst (split ls (+ (length ls) n))))
           (append (cadr lst) (car lst))))))
