// Jumping through Hyperspace ain't like dusting Crops

use std::collections::TreeMap;
use std::collections::DList;

struct Graph {
    graph: TreeMap<char, TreeSet<Edge>>,
}

impl Graph {
    fn new() -> Graph { Graph{graph: TreeMap::new(),} }
    fn insert(&mut self, src: char, dest: char, weight: uint) {
        self.insert_node(src, dest, weight);
        self.insert_node(dest, src, weight);
    }
    fn insert_node(&mut self, src: char, dest: char, weight: uint) {
        if !self.graph.contains_key(&src) {
            self.graph.insert(src, TreeSet::new());
        }
        let set = self.graph.find_mut(&src);
        match set { 
            Some(x) => { x.insert(Edge::new(dest, weight)); }, 
            None => {} 
        }
    }
    fn print(&self) {
        for (src, edges) in self.graph.iter() {
            for edge in edges.iter() {
                println!("{} - {} - {}", src, edge.vertex, edge.weight);
            }
        }
    }
    
    fn bfs(&self, dest: char) -> TreeSet<uint> {
        let mut set : TreeSet<uint> = TreeSet::new();
        let mut q: DList<char> = DList::new();
        let mut weight : uint = 0;
        q.push('A')
        while(!q.is_empty()) {
            let node = q.pop_front();
            if node == dest {
                set.insert(weight);
            }
            for edge in self.graph.find(&node) {
                if edge.vertex 
            }
        }
    }
    
    fn search_path(&self) {
        let mut distances : TreeMap<char, TreeSet<uint>> = TreeMap::new();
        let planets_to_visit : Vec<char> = self.graph.keys().filter_map(|&x| if x != 'A' {Some(x)} else {None}).collect();
    }
}
