-- Bogosort
-- http://www.reddit.com/r/dailyprogrammer/comments/2d8yk5/8112014_challenge_175_easy_bogo/

-- Convolute the input string and return the new string
function randomSort(s)
  local t = {}
  for i = 1, s:len() do
    t[i] = {math.random(), s:byte(i)}
  end
  table.sort(t, compare)

  local ret = {}
  for k, v in pairs(t) do
    table.insert(ret, string.char(v[2]))
  end
  return table.concat(ret, "")
end

--sort callback
function compare(a, b) return a[1] < b[1] end

--Sorts a string randomly until it matches the output and returns the number
--of iterations it took to get there
function bogo(input, output)
  local i = 0
  while(input:lower() ~= output:lower()) do
    input = randomSort(input)
    i = i + 1
  end
  return i
end

function main()
  math.randomseed(os.time())
  io.write("Input string: ")
  local input = io.read()
  io.write("Output string: ")
  local output = io.read()
  local i = bogo(input, output)
  io.write(i, " iterations \n")
end

main()
