<#
.SYNOPSIS
    Script to convert images to grayscale bitmaps. Daily Programmer Challenge #179 Easy.
.PARAMETER display
    Will cause the script to display the image immediately instead of saving
    it to the current directory.
.INPUTS
    The script takes a path to the image file to be converted
.OUTPUTS
    A bitmap to the directory of the original file if not displayed.
.LINK
    http://www.reddit.com/r/dailyprogrammer/comments/2ftcb8/9082014_challenge_179_easy_you_make_me_happy_when/
#>

Param(
    [Parameter(Mandatory=$true)][string]$path = $(Read-Host "Provide a path to the image file"),
    [Switch]$display = $false
)

[System.Reflection.Assembly]::LoadWithPartialName("System.Drawing")
    
function Display-Image($img) {
    [void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
    [System.Windows.Forms.Application]::EnableVisualStyles()
    
    $form = New-Object Windows.Forms.Form
    $form.Text = "Grayscale Image"
    $form.Width = $img.width
    $form.Height = $img.height
    
    $box = New-Object Windows.Forms.PictureBox
    $box.Width = $img.width
    $box.Height = $img.height
    
    $box.Image = $img
    $form.controls.add($box)
    #$forms.Add_Shown({$form.Activate()})
    $form.ShowDialog()
}

#Returns a System.Drawing.Bitmap object
function Convert-Grayscale($path) {
    try {
        $img = New-Object System.Drawing.Bitmap($path)
    }
    catch {
        Write-Host "Invalid image file"
        Exit
    }

    $new_img = New-Object System.Drawing.Bitmap($img.width, $img.height)

    for ($i = 0; $i -lt $img.width; $i++) {
        for ($j = 0; $j -lt $img.height; $j++) {
            # Luminosity forumla: 0.21R + 0.72G + 0.07 B
            $pix = $img.GetPixel($i, $j)
            $gray = 0.21*$pix.R + 0.72*$pix.G + 0.07*$pix.B
            $new_img.SetPixel($i, $j, [System.Drawing.Color]::FromArgb($gray, $gray, $gray))
        }
    }
    return $new_img
}

function main() {
    $img = Convert-Grayscale $path

    if($display) {
        Display-Image $img
    }
    else {
        $name = (Split-Path $path -leaf).split('.')[0]
        $save_path = (Split-Path $path -parent) + '\' + $name + '_grayscale.bmp'
        $img.Save($save_path, "BMP")
    }
}

# Run everything
main
