// Challenge # 178 [Easy]
// http://www.reddit.com/r/dailyprogrammer/comments/2f6a7b/9012014_challenge_178_easy_transformers_matrices/
// Written using rust-0.11

use std::num;
use std::io;
use std::f64;

// Struct to contain the starting point
struct Point {
    x: f64,
    y: f64
}

// Implemenation of point methods
impl Point {
    fn new(x: f64, y: f64) -> Point {
        Point { x: x, y: y }
    }
    
    fn translate(&mut self, a: f64, b: f64) {
        self.x += a;
        self.y += b;
    }

    fn rotate(&mut self, a: f64, b: f64, c: f64) {
        let z = (num::pow(self.x - a, 2) + num::pow(self.y - b, 2)).sqrt();
        let ang = (self.y - b).atan2(self.x - a) - c;
        self.y = ang.sin()*z +b;
        self.x = ang.cos()*z +a;
    }

    fn scale(&mut self, a: f64, b: f64, c: f64) { 
        let z = (num::pow(self.x - a, 2) + num::pow(self.y - b, 2)).sqrt();
        let cz = z*c;
        let ang = (self.y - b).atan2(self.x - a);
        self.y = ang.sin()*cz + b;
        self.x = ang.cos()*cz + a;
    }

    fn reflect(&mut self, axis: char) {
        if axis == 'x' {
            self.y *= -1.0;
        }
        else if axis == 'y' {
            self.x *= -1.0;
        }
    }

    fn finish(&self) {
        println!("({0}, {1})", self.x, self.y);
    }
}

fn main() {
    let mut first = true;
    let mut p: Point = Point::new(0.0, 0.0); 
    for line in io::stdin().lines() {
        let v: String = line.unwrap();
        let s: &str = v.as_slice();
        let args: Vec<f64> = s.split(|c: char| !c.is_digit())
            .filter_map(from_str)
            .collect();
        if first {
            p = Point::new(*args.get(0), *args.get(1));
            first = false;
        }
        else {
         let func: Vec<&str> = s.split('(').collect();
         match *func.get(0) {
             "translate" => p.translate(*args.get(0), *args.get(1)),
             "rotate" => p.rotate(*args.get(0), *args.get(1),* args.get(2)),
             "scale" => p.scale(*args.get(0), *args.get(1), *args.get(2)),
             "reflect" => if s.contains_char('X') {
                 p.reflect('x');
             }
             else {
                 p.reflect('y');
             },
             _ => {p.finish(); break;},
         }

        }
        p.finish();
    }
}


#[test]
fn test_point_translate() {
    let mut p = Point::new(1.0, 1.0);
    p.translate(1.0, -1.0);
    assert!(p.x == 2.0 && p.y == 0.0);
}

#[test]
fn test_point_rotate_ninety() {
    let mut p = Point::new(1.0, 0.0);
    p.rotate(0.0, 0.0, 0.5*f64::consts::PI);
    assert!(p.x.round() == 0.0 && p.y.round() == -1.0,
            "{0}, {1} expected 0, -1", p.x, p.y);
}

#[test]
fn test_point_rotate_negative_ninety() {
    let mut p = Point::new(0.0, 1.0);
    p.rotate(0.0, 0.0, -0.5*f64::consts::PI);
    assert!(p.x.round() == -1.0 && p.y.round() == 0.0, 
            "{0}, {1} expected -1, 0", p.x, p.y);
}

#[test]
fn test_point_scale_one() {
    let mut p = Point::new(0.0, 1.0);
    p.scale(0.0, 0.0, 1.0);
    assert!(p.x == 0.0 && p.y == 1.0,
            "{0}, {1} expected -1, 0", p.x, p.y);
            
}

#[test]
fn test_point_scale_by_three() {
    let mut p = Point::new(0.0, 1.0);
    p.scale(0.0, 0.0, 3.0);
    assert!(p.x == 0.0 && p.y == 3.0,
            "{0}, {1} expected -1, 0", p.x, p.y);
            
}

#[test]
fn test_point_scale_negative() {
    let mut p = Point::new(0.0, 1.0);
    p.scale(0.0, 0.0, -0.25);
    assert!(p.x == 0.0 && p.y == -0.25,
            "{0}, {1} expected -1, 0", p.x, p.y);

}

#[test]
fn test_point_scale_by_half() {
    let mut p = Point::new(0.0, 1.0);
    p.scale(0.0, 0.0, 0.5);
    assert!(p.x == 0.0 && p.y == 0.5,
            "{0}, {1} expected -1, 0", p.x, p.y);

}

#[test]
fn test_point_reflect_x() {
    let mut p = Point::new(0.0, 1.0);
    p.reflect('x');
    assert!(p.x == 0.0 && p.y == -1.0);
}

#[test]
fn test_point_reflect_y() {
    let mut p = Point::new(1.0, 0.0);
    p.reflect('y');
    assert!(p.x == -1.0 && p.y == 0.0);
}

#[test]
fn test_point_movement() {
    let mut p = Point::new(0.0, 5.0);
    p.translate(3.0, 2.0);
    p.scale(1.0,3.0,0.5);
    p.rotate(3.0,2.0,1.57079632679);
    p.reflect('x');
    p.translate(2.0,-1.0);
    p.scale(0.0,0.0,-0.25);
    p.rotate(1.0,-3.0,3.14159265359);
    p.reflect('y');
    assert!(p.x == -1.0 && p.y == 0.0,
            "{0}, {1} expected -4, -7", p.x, p.y);
}
