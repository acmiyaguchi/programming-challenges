-- Find the largest word
-- http://www.reddit.com/r/dailyprogrammer/comments/2dgd5v/8132014_challenge_175_intermediate_largest_word/

function printTable(t)
  local w = {}
  for _, v in pairs(t) do
    local word = {}
    for _, vv in pairs(v) do
      table.insert(word, string.char(vv))
    end
    table.insert(w, table.concat(word))
  end
  print(table.concat(w, " "))
end

function testStrings(s, test)
  local t = {}
  for _, v in pairs(s) do if t[v] then t[v] = t[v] + 1 else t[v] = 1 end end
  for _, v in pairs(test) do if t[v[1]] then t[v[1]] = t[v[1]] - 1 end end
  for _, v in pairs(t) do if v > 0 then return false end end
  return true
end

function parseString(s)
  local list = {}
  local word = {}
  for i=1, #s do
    if s:byte(i) == string.byte(' ') then
      if #word ~= 0 then
        table.insert(list, word)
        word = {}
      end
      goto continue
    end
    table.insert(word, s:byte(i))
    ::continue::
  end
  if #word ~= 0 then table.insert(list, word) end
  return list
end

function parseInput(word, char)
  local words = parseString(word)
  local chars = parseString(char)

  local t = {}
  local len = 0
  for _, v in pairs(words) do
    if testStrings(v, chars) then
      if #v > len then
        len = #v
        t = {v}
      elseif #v == len then table.insert(t, v) end
    end
  end
  printTable(t)
end

function main()
  io.write("String 1: ")
  local word = io.read()
  io.write("String 2: ")
  local char = io.read()
  parseInput(word, char)
end

main()
--[[
    sample output: abc bca
    challenge 1 output: mellow yellow fellow
    challenge 2 output: 
--]]
