'''
Anthony Miyaguchi
March 3, 2015

520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
'''

def main():
    acc = 1;
    for i in range(2, 21):
        div = i
        for j in range(2, i):
            a = j
            for k in range(2, j):
                if a % k == 0:
                    a /= k
            if div % a == 0:
                div /= a
        print(div)
        acc *= div
    print(acc)

if __name__ == '__main__':
    main()