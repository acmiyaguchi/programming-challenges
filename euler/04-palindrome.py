'''
Anthony Miyaguchi
March 2, 2015

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
'''

def is_palindrome(n):
    s = str(n)
    for k, v in enumerate(s):
        if v != s[-(k+1)]:
            return False
    return True
        
def main():
    largest = 0
    for i in range(100, 999):
        for j in range(i, 999):
            k = i* j
            if is_palindrome(k) and k > largest:
                largest = k
    print(largest)

if __name__ == '__main__':
    main()
